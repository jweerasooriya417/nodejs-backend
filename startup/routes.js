const express = require('express');
const menus = require('../routes/menu');
const categories = require('../routes/categories');
const carts = require('../routes/cart');
const orders = require('../routes/order');

module.exports = function(app) {
  app.use(express.json());
  app.use('/api/menus', menus);
  app.use('/api/category', categories);
  app.use('/api/carts', carts);
  app.use('/api/orders', orders);

//   app.use(error);
}
