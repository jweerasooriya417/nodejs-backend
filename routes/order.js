const {Order} = require('../models/order');
const express = require('express');
const router =  express.Router();


//To prevent errors from Cross Origin Resource Sharing, we will set
//our headers to allow CORS with middleware like so:
router.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Headers",
        "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    //and remove cacheing so we get the most recent comments
    res.setHeader("Cache-Control", "no-cache");
    next();
});


router.get('/', async(req, res) => {
    const result = await Order.find();
    res.send(result);
});

router.post('/', async(req, res) => {
    try {
        console.log("creating new order ... " , req.body);
        let order = new Order({
            datePlaced: new Date(),

            items: {
                totalPrice: req.body.items.totalPrice,
                products: req.body.items.cart,
            },
            shipping: {
                firstName: req.body.shipping.firstName,
                lastName: req.body.shipping.lastName,
                email: req.body.shipping.email,
                address: {
                    address1: req.body.shipping.add1,
                    address2: req.body.shipping.add2,
                    state: req.body.shipping.state,
                    country: req.body.shipping.country,
                    zip: req.body.shipping.zip
                },

                payment: {
                    cardNum: req.body.shipping.cardNumber,
                    cardExp: req.body.shipping.cardExp,
                    cardHolderName: req.body.shipping.cardHolder
                }
            }
        });
        const newOrder = await order.save();
        console.log("created new order", newOrder);
        res.status(200).send(newOrder);
    }
    catch (ex) {
        console.log(ex);
        res.send(ex);
    }
});


module.exports = router;





