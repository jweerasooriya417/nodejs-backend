const {Menu, validate} = require('../models/menu');
const express = require('express');
const router =  express.Router();
const Joi = require('joi');

//To prevent errors from Cross Origin Resource Sharing, we will set 
//our headers to allow CORS with middleware like so:
router.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Headers", 
     "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
   //and remove cacheing so we get the most recent comments
    res.setHeader("Cache-Control", "no-cache");
    next();
});


router.get('/', async (req, res) => {
    
    console.log("getting categories ...");
    // const categories = await Menu
    //         .find()
    //         .distinct('category');
    //
    const categories = ["Western", "Chinese", "Dessert", "Japanese", "Beverages"];
    res.send(categories);
});


module.exports = router;
