const env = require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');

const app = express();
require("./startup/routes")(app);


const DB_URL = process.env.DB_PATH;
const APP_PORT = process.env.APP_PORT;

mongoose.connect(`${DB_URL}`,  {useNewUrlParser: true })
    .then(() => console.log('Connected to MongoDB...'))
    .catch (err => console.error("Failed to connect to DB ..", err));

app.listen(APP_PORT, () => console.log(`listening on port ${APP_PORT}... `));


