const mongoose = require('mongoose');
const Joi = require('joi');


//compile schema into model
const Order = mongoose.model('Order', new mongoose.Schema({
    datePlaced: {
        type: Date,
    },
    status: {
        type: String,
        enum: ["Pending", "Done"],
        default: "Pending"
    },
    items: {
        products: {
            type: Array,
            object: {
                name: {
                    type: String,
                    required: true
                },
                price: {
                    type: String,
                    required: true
                },
                imageUrl: {
                    type: String,
                    required: true
                },
                quantity: {
                    type: Number,
                    require: true,
                    default: 0
                },
            }
        },
        totalPrice: {
            type: String,
            required: true
        }
    },
    shipping: {
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        address: {
            address1: {
                type: String,
                required: true
            },
            address2: {
                type: String
            },
            state: {
                type: String,
                required: true
            },
            country: {
                type: String,
                required: true
            },
            zip: {
                type: String,
                required: true
            }
        },
        payment: {
            cardNum: {
                type: String,
                required: true
            },
            cardHolderName: {
                type: String,
                required: true
            },
            cardExp: {
                type: String,
                required: true
            }
        }
    }
}));


exports.Order = Order;
